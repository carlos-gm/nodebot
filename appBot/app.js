/* 
  App2Bot by SeXWaX
*/

//READ -> https://masteringmean.com/lessons/53-Communicating-with-TCP-in-Nodejs

var SYSTEM_NAME    = "App2Bot";
var HTTP_PORT = "8080";
var WEBSOCKET_PORT = 5001; // Web (websockets)

var app = require('express')()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server)
  , express = require('express')
  , net = require('net');

server.listen(HTTP_PORT);
console.log(SYSTEM_NAME+' listening on port '+HTTP_PORT);

app.get('/', function (req, res) {
  res.sendfile(__dirname + '/html/index.html');  
});

app.get('/bot.html', function (req, res) {
  res.sendfile(__dirname + '/html/bot/index.html');  
});

app.get('/car.html', function (req, res) {
  res.sendfile(__dirname + '/html/car/index.html');  
});

// Conexión websocket
var appWS = require('http').createServer(handler)
  , io = require('socket.io').listen(appWS)
  , fs = require('fs')

appWS.listen(WEBSOCKET_PORT);

function handler (req, res) {
  fs.readFile(__dirname + '/html/index.html',
  function (err, data) {
    if (err) {
      res.writeHead(500);
      return res.end('Error loading index.html');
    }
    res.writeHead(200);
    res.end(data);
  });
}

// client
var Raspberry = require('net').Socket();

//esta funcion envia el socket a la Raspberry
function sendRaspberry(data){
    Raspberry.connect(5002);
    Raspberry.write(data);
    Raspberry.end();
}

var btn_up = 0;
var btn_dw = 0;
var btn_lf = 0;
var btn_rg = 0;

io.sockets.on('connection', function (socket) {

  socketWeb = socket;  

  socket.on('btn', function (data) {

    if(data.btn == "btn_up"){
      btn_up++;
      data = 1;
    }

    if(data.btn == "btn_dw"){
      btn_dw++;
      data = 2;
    }

    console.log("btn_up:" + data.btn_up);
    socketWeb.emit('status', { 'btn_up': btn_up, 'btn_dw': btn_dw, 'btn_lf': btn_lf, 'btn_rg': btn_rg});
    sendRaspberry("btn -> " + data);
    //socket.write('You said "' + data + '"');
  });

  socket.on('status', function (data) { 
    console.log("status: waiting...");
    socketWeb.emit('status', { 'btn_up': btn_up, 'btn_dw': btn_dw, 'btn_lf': btn_lf, 'btn_rg': btn_rg});
  });

  socket.on('conexion', function (data) {
    console.log('Websocket conectado')      
    socketWeb.emit('status', { valor: 1});
  });  

/* EL ROBOT*/
/*
require('net').createServer(function (socket) {
    console.log("connected");
    socket.on('data', function (data) {
        console.log(data.toString());
    });
}).listen(5002);
*/


});